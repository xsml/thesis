#include "stdafx.h"
#include "Score.h"

Score::Score()
{
	noteon_= 0;
	pitch_ = 0;
	velocity_ = 0;
	artdur_ = 0;
	channel_ = 0;
}
Score::Score(short noteon, short pitch, short velocity, short artdur, short channel)
{
	noteon_= noteon;
	pitch_ = pitch;
	velocity_ = velocity;
	artdur_ = artdur;
	channel_ = channel;
}
void Score::create()
{
    // create an output file in .sc format:
	std::ofstream tfile("Output.sc");
    // write the head of the output file:
	tfile<<"File-ID : 2"<<std::endl;
	tfile<<"Format : 1"<<std::endl;
}
void Score::write()
{
    // append the note to the output file
	std::ofstream tfile("Output.sc", std::ios::app);
    // write the note
	tfile<<noteon_<<' '<<pitch_<<' '<<velocity_<<' '<<artdur_<<' '<<channel_<<std::endl;
}
void Score::finish()
{
    // write the end flag of the output file:
	std::ofstream tfile("Output.sc", std::ios::app);
	tfile<<"0 -1 0 0 0"<<std::endl;
}
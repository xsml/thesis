#include "stdafx.h"
#include "Frequency.h"

Frequency::Frequency()
{
	mode_ = 0;
	tonic_ = 0;
	susceptibility_ = 0;
	centerPitch_ = 0;
	presentPitch_ = 0;
	nextPitch_ = 0;
	centerPitchClass_ = 0;
	presentPitchClass_ = 0;
	nextPitchClass_ = 0;
	centerOctave_ = 0;
	presentOctave_ = 0;
	nextOctave_ = 0 ;
	centerFrequency_ = 0;
	presentFrequency_ = 0;
	nextFrequency_ = 0;
	presentFrequencyRatio_ = 0;
	nextFrequencyRatio_ = 0;
	presentEnergyRatio_ = 0;
	nextEnergyRatio_ = 0;
	energyRatioInterval_ = 0;
}
Frequency::Frequency(short mode, short tonic, short susceptibility,
					 short centerPitch, short presentPitch, short nextPitch)
{
	mode_ = mode;
	tonic_ = tonic;
	susceptibility_ = susceptibility;
	centerPitch_ = centerPitch;
	presentPitch_ = presentPitch;
	nextPitch_ = nextPitch;
	centerPitchClass_ = (centerPitch_ + 12 - tonic_) % 12;
	presentPitchClass_ = (presentPitch_ + 12 - tonic_) % 12;
	nextPitchClass_ = (nextPitch_ + 12 - tonic_) % 12;
	centerOctave_ = (centerPitch_ + 12) / 12 - 1;
	presentOctave_ = (presentPitch_ + 12) / 12 - 1;
	nextOctave_ = (nextPitch_ + 12) / 12 - 1;
	centerFrequency_ = allocateFrequency(centerOctave_, centerPitchClass_);
	presentFrequency_ = allocateFrequency(presentOctave_, presentPitchClass_);
	nextFrequency_ = allocateFrequency(nextOctave_, nextPitchClass_);
	presentFrequencyRatio_ = presentFrequency_ / centerFrequency_;
	nextFrequencyRatio_ = nextFrequency_ / centerFrequency_;
	presentEnergyRatio_ = presentFrequencyRatio_ * presentFrequencyRatio_;
	nextEnergyRatio_ = nextFrequencyRatio_ * nextFrequencyRatio_;
	energyRatioInterval_ = nextEnergyRatio_ - presentEnergyRatio_;
}
double Frequency::allocateFrequency(short octave, short pitchClass)
{
	double frequency = 0;
	switch(mode_)
	{
		case 1 : // Ionian
			switch(pitchClass)
			{
				case  0 : frequency = 24 * pow(2.0, octave); break;
				case  2 : frequency = 27 * pow(2.0, octave); break;
				case  4 : frequency = 30 * pow(2.0, octave); break;
				case  5 : frequency = 32 * pow(2.0, octave); break;
				case  7 : frequency = 36 * pow(2.0, octave); break;
				case  9 : frequency = 40 * pow(2.0, octave); break;
				case 11 : frequency = 45 * pow(2.0, octave); break;
				default : frequency = 1; break;
			}
			break;
		case 2 : // Aeolian
			switch(pitchClass)
			{
				case  0 : frequency = 120 * pow(2.0, octave); break;
				case  2 : frequency = 135 * pow(2.0, octave); break;
				case  3 : frequency = 144 * pow(2.0, octave); break;
				case  5 : frequency = 160 * pow(2.0, octave); break;
				case  7 : frequency = 180 * pow(2.0, octave); break;
				case  8 : frequency = 192 * pow(2.0, octave); break;
				case 10 : frequency = 216 * pow(2.0, octave); break;
				default : frequency = 2; break;
			}
			break;
		default : frequency = 3; break;
	}
	return frequency;
}
double Frequency::countEnergyRatioMax(double energyRatioMax)
{
	energyRatioMax = energyRatioMax - (energyRatioInterval_ * susceptibility_); // next Max
	if(energyRatioMax < 1)
		energyRatioMax = 1; // Max always >=1
	return energyRatioMax;
}
double Frequency::countEnergyRatioMin(double energyRatioMin)
{
	double reciprocalEnergyRatioMin = 1 / energyRatioMin; // present rMin
	reciprocalEnergyRatioMin = reciprocalEnergyRatioMin + (energyRatioInterval_ * susceptibility_); // next rMin
	if(reciprocalEnergyRatioMin < 1)
		reciprocalEnergyRatioMin = 1; // rMin always >=1
	energyRatioMin = 1 / reciprocalEnergyRatioMin; // next Min
	return energyRatioMin;
}
double Frequency::getNextEnergyRatio()
{
	return nextEnergyRatio_;
}
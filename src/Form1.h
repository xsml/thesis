#pragma once
#include "Note.h"
#include "Score.h"

namespace Melody {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Form1 的摘要
	///
	/// 警告: 如果您變更這個類別的名稱，就必須變更與這個類別所依據之所有 .resx 檔案關聯的
	///          Managed 資源編譯器工具的 'Resource File Name' 屬性。
	///          否則，這些設計工具
	///          將無法與這個表單關聯的當地語系化資源
	///          正確互動。
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: 在此加入建構函式程式碼
			//
		}

	protected:
		/// <summary>
		/// 清除任何使用中的資源。
		/// </summary>
		~Form1()
		{
			if(components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TrackBar^  trackBar_nNote;
	private: System::Windows::Forms::TrackBar^  trackBar_bottom;
	private: System::Windows::Forms::TrackBar^  trackBar_range;
	private: System::Windows::Forms::TrackBar^  trackBar_mode;
	private: System::Windows::Forms::TrackBar^  trackBar_tonic;
	private: System::Windows::Forms::TrackBar^  trackBar_duration;
	private: System::Windows::Forms::TrackBar^  trackBar_center;
	private: System::Windows::Forms::TrackBar^  trackBar_susceptibility;
	private: System::Windows::Forms::TrackBar^  trackBar_similiarStepMax;
	private: System::Windows::Forms::TrackBar^  trackBar_contraryStepMax;
	private: System::Windows::Forms::TrackBar^  trackBar_stepMax;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::TrackBar^  trackBar_skipMax;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::TextBox^  textBox_nNote;
	private: System::Windows::Forms::TextBox^  textBox_bottom;
	private: System::Windows::Forms::TextBox^  textBox_range;
	private: System::Windows::Forms::TextBox^  textBox_tonic;
	private: System::Windows::Forms::TextBox^  textBox_duration;
	private: System::Windows::Forms::TextBox^  textBox_center;
	private: System::Windows::Forms::TextBox^  textBox_susceptibility;
	private: System::Windows::Forms::TextBox^  textBox_similiarStepMax;
	private: System::Windows::Forms::TextBox^  textBox_contraryStepMax;
	private: System::Windows::Forms::TextBox^  textBox_stepMax;
	private: System::Windows::Forms::TextBox^  textBox_skipMax;
	private: System::Windows::Forms::TextBox^  textBox_mode;
	private: System::Windows::Forms::Button^  button_execute;

	private: System::Windows::Forms::TrackBar^  trackBar_countMax;
	private: System::Windows::Forms::TextBox^  textBox_countMax;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::TrackBar^  trackBar_repetitionMax;
	private: System::Windows::Forms::TextBox^  textBox_repetitionMax;
	private: System::Windows::Forms::Label^  label14;
	private:
		/// <summary>
		/// 設計工具所需的變數。
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
		///
		/// </summary>
		void InitializeComponent(void)
		{
			this->trackBar_nNote = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_bottom = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_range = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_mode = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_tonic = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_duration = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_center = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_susceptibility = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_similiarStepMax = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_contraryStepMax = (gcnew System::Windows::Forms::TrackBar());
			this->trackBar_stepMax = (gcnew System::Windows::Forms::TrackBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->trackBar_skipMax = (gcnew System::Windows::Forms::TrackBar());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->textBox_nNote = (gcnew System::Windows::Forms::TextBox());
			this->textBox_bottom = (gcnew System::Windows::Forms::TextBox());
			this->textBox_range = (gcnew System::Windows::Forms::TextBox());
			this->textBox_mode = (gcnew System::Windows::Forms::TextBox());
			this->textBox_tonic = (gcnew System::Windows::Forms::TextBox());
			this->textBox_duration = (gcnew System::Windows::Forms::TextBox());
			this->textBox_center = (gcnew System::Windows::Forms::TextBox());
			this->textBox_susceptibility = (gcnew System::Windows::Forms::TextBox());
			this->textBox_similiarStepMax = (gcnew System::Windows::Forms::TextBox());
			this->textBox_contraryStepMax = (gcnew System::Windows::Forms::TextBox());
			this->textBox_stepMax = (gcnew System::Windows::Forms::TextBox());
			this->textBox_skipMax = (gcnew System::Windows::Forms::TextBox());
			this->button_execute = (gcnew System::Windows::Forms::Button());
			this->trackBar_countMax = (gcnew System::Windows::Forms::TrackBar());
			this->textBox_countMax = (gcnew System::Windows::Forms::TextBox());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->trackBar_repetitionMax = (gcnew System::Windows::Forms::TrackBar());
			this->textBox_repetitionMax = (gcnew System::Windows::Forms::TextBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_nNote))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_bottom))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_range))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_mode))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_tonic))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_duration))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_center))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_susceptibility))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_similiarStepMax))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_contraryStepMax))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_stepMax))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_skipMax))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_countMax))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_repetitionMax))->BeginInit();
			this->SuspendLayout();
			// 
			// trackBar_nNote
			// 
			this->trackBar_nNote->Location = System::Drawing::Point(12, 15);
			this->trackBar_nNote->Maximum = 32;
			this->trackBar_nNote->Minimum = 8;
			this->trackBar_nNote->Name = L"trackBar_nNote";
			this->trackBar_nNote->Size = System::Drawing::Size(272, 42);
			this->trackBar_nNote->TabIndex = 101;
			this->trackBar_nNote->Value = 10;
			this->trackBar_nNote->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_nNote_Scroll);
			// 
			// trackBar_bottom
			// 
			this->trackBar_bottom->Location = System::Drawing::Point(12, 45);
			this->trackBar_bottom->Maximum = 79;
			this->trackBar_bottom->Minimum = 24;
			this->trackBar_bottom->Name = L"trackBar_bottom";
			this->trackBar_bottom->Size = System::Drawing::Size(272, 42);
			this->trackBar_bottom->TabIndex = 102;
			this->trackBar_bottom->Value = 48;
			this->trackBar_bottom->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_bottom_Scroll);
			// 
			// trackBar_range
			// 
			this->trackBar_range->Location = System::Drawing::Point(12, 78);
			this->trackBar_range->Maximum = 48;
			this->trackBar_range->Minimum = 12;
			this->trackBar_range->Name = L"trackBar_range";
			this->trackBar_range->Size = System::Drawing::Size(272, 42);
			this->trackBar_range->TabIndex = 103;
			this->trackBar_range->Value = 24;
			this->trackBar_range->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_range_Scroll);
			// 
			// trackBar_mode
			// 
			this->trackBar_mode->Location = System::Drawing::Point(12, 112);
			this->trackBar_mode->Maximum = 2;
			this->trackBar_mode->Minimum = 1;
			this->trackBar_mode->Name = L"trackBar_mode";
			this->trackBar_mode->Size = System::Drawing::Size(272, 42);
			this->trackBar_mode->TabIndex = 104;
			this->trackBar_mode->Value = 1;
			this->trackBar_mode->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_mode_Scroll);
			// 
			// trackBar_tonic
			// 
			this->trackBar_tonic->Location = System::Drawing::Point(12, 144);
			this->trackBar_tonic->Maximum = 11;
			this->trackBar_tonic->Name = L"trackBar_tonic";
			this->trackBar_tonic->Size = System::Drawing::Size(272, 42);
			this->trackBar_tonic->TabIndex = 105;
			this->trackBar_tonic->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_tonic_Scroll);
			// 
			// trackBar_duration
			// 
			this->trackBar_duration->LargeChange = 120;
			this->trackBar_duration->Location = System::Drawing::Point(12, 188);
			this->trackBar_duration->Maximum = 960;
			this->trackBar_duration->Minimum = 12;
			this->trackBar_duration->Name = L"trackBar_duration";
			this->trackBar_duration->Size = System::Drawing::Size(272, 42);
			this->trackBar_duration->SmallChange = 12;
			this->trackBar_duration->TabIndex = 106;
			this->trackBar_duration->Value = 120;
			this->trackBar_duration->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_duration_Scroll);
			// 
			// trackBar_center
			// 
			this->trackBar_center->LargeChange = 10;
			this->trackBar_center->Location = System::Drawing::Point(12, 218);
			this->trackBar_center->Maximum = 116;
			this->trackBar_center->Minimum = 12;
			this->trackBar_center->Name = L"trackBar_center";
			this->trackBar_center->Size = System::Drawing::Size(272, 42);
			this->trackBar_center->TabIndex = 107;
			this->trackBar_center->Value = 60;
			this->trackBar_center->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_center_Scroll);
			// 
			// trackBar_susceptibility
			// 
			this->trackBar_susceptibility->Location = System::Drawing::Point(12, 249);
			this->trackBar_susceptibility->Maximum = 4;
			this->trackBar_susceptibility->Minimum = 1;
			this->trackBar_susceptibility->Name = L"trackBar_susceptibility";
			this->trackBar_susceptibility->Size = System::Drawing::Size(272, 42);
			this->trackBar_susceptibility->TabIndex = 108;
			this->trackBar_susceptibility->Value = 2;
			this->trackBar_susceptibility->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_susceptibility_Scroll);
			// 
			// trackBar_similiarStepMax
			// 
			this->trackBar_similiarStepMax->Location = System::Drawing::Point(12, 292);
			this->trackBar_similiarStepMax->Maximum = 8;
			this->trackBar_similiarStepMax->Name = L"trackBar_similiarStepMax";
			this->trackBar_similiarStepMax->Size = System::Drawing::Size(272, 42);
			this->trackBar_similiarStepMax->TabIndex = 109;
			this->trackBar_similiarStepMax->Value = 4;
			this->trackBar_similiarStepMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_similiarStepMax_Scroll);
			// 
			// trackBar_contraryStepMax
			// 
			this->trackBar_contraryStepMax->Location = System::Drawing::Point(12, 326);
			this->trackBar_contraryStepMax->Maximum = 8;
			this->trackBar_contraryStepMax->Name = L"trackBar_contraryStepMax";
			this->trackBar_contraryStepMax->Size = System::Drawing::Size(272, 42);
			this->trackBar_contraryStepMax->TabIndex = 110;
			this->trackBar_contraryStepMax->Value = 1;
			this->trackBar_contraryStepMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_contraryStepMax_Scroll);
			// 
			// trackBar_stepMax
			// 
			this->trackBar_stepMax->Location = System::Drawing::Point(12, 358);
			this->trackBar_stepMax->Maximum = 8;
			this->trackBar_stepMax->Name = L"trackBar_stepMax";
			this->trackBar_stepMax->Size = System::Drawing::Size(272, 42);
			this->trackBar_stepMax->TabIndex = 111;
			this->trackBar_stepMax->Value = 5;
			this->trackBar_stepMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_stepMax_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(330, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(66, 12);
			this->label1->TabIndex = 301;
			this->label1->Text = L"Pitch amount";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(330, 45);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(40, 12);
			this->label2->TabIndex = 302;
			this->label2->Text = L"Bottom";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(330, 78);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 12);
			this->label3->TabIndex = 303;
			this->label3->Text = L"Range";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(330, 112);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(128, 12);
			this->label4->TabIndex = 304;
			this->label4->Text = L"Mode: 1.Ionian, 2.Aeolian";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(330, 144);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(122, 12);
			this->label5->TabIndex = 305;
			this->label5->Text = L"Tonic (0 = C, ..., 11 = B)";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(330, 188);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(138, 12);
			this->label6->TabIndex = 306;
			this->label6->Text = L"Duration (480 = whole note)";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(330, 218);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(62, 12);
			this->label7->TabIndex = 307;
			this->label7->Text = L"Center pitch";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(330, 249);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(67, 12);
			this->label8->TabIndex = 308;
			this->label8->Text = L"Susceptibility";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(330, 292);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(205, 12);
			this->label9->TabIndex = 309;
			this->label9->Text = L"Consecutive similiar step time(s) maximum";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(330, 326);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(210, 12);
			this->label10->TabIndex = 310;
			this->label10->Text = L"Consecutive contrary step time(s) maximum";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(330, 358);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(168, 12);
			this->label11->TabIndex = 311;
			this->label11->Text = L"Consecutive step time(s) maximum";
			// 
			// trackBar_skipMax
			// 
			this->trackBar_skipMax->Location = System::Drawing::Point(12, 388);
			this->trackBar_skipMax->Maximum = 8;
			this->trackBar_skipMax->Name = L"trackBar_skipMax";
			this->trackBar_skipMax->Size = System::Drawing::Size(272, 42);
			this->trackBar_skipMax->TabIndex = 112;
			this->trackBar_skipMax->Value = 1;
			this->trackBar_skipMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_skipMax_Scroll);
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(330, 388);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(169, 12);
			this->label12->TabIndex = 312;
			this->label12->Text = L"Consecutive skip time(s) maximum";
			// 
			// textBox_nNote
			// 
			this->textBox_nNote->Location = System::Drawing::Point(283, 17);
			this->textBox_nNote->Name = L"textBox_nNote";
			this->textBox_nNote->ReadOnly = true;
			this->textBox_nNote->Size = System::Drawing::Size(41, 22);
			this->textBox_nNote->TabIndex = 201;
			// 
			// textBox_bottom
			// 
			this->textBox_bottom->Location = System::Drawing::Point(283, 45);
			this->textBox_bottom->Name = L"textBox_bottom";
			this->textBox_bottom->ReadOnly = true;
			this->textBox_bottom->Size = System::Drawing::Size(41, 22);
			this->textBox_bottom->TabIndex = 202;
			// 
			// textBox_range
			// 
			this->textBox_range->Location = System::Drawing::Point(283, 78);
			this->textBox_range->Name = L"textBox_range";
			this->textBox_range->ReadOnly = true;
			this->textBox_range->Size = System::Drawing::Size(41, 22);
			this->textBox_range->TabIndex = 203;
			// 
			// textBox_mode
			// 
			this->textBox_mode->Location = System::Drawing::Point(283, 112);
			this->textBox_mode->Name = L"textBox_mode";
			this->textBox_mode->ReadOnly = true;
			this->textBox_mode->Size = System::Drawing::Size(41, 22);
			this->textBox_mode->TabIndex = 204;
			// 
			// textBox_tonic
			// 
			this->textBox_tonic->Location = System::Drawing::Point(283, 144);
			this->textBox_tonic->Name = L"textBox_tonic";
			this->textBox_tonic->ReadOnly = true;
			this->textBox_tonic->Size = System::Drawing::Size(41, 22);
			this->textBox_tonic->TabIndex = 205;
			// 
			// textBox_duration
			// 
			this->textBox_duration->Location = System::Drawing::Point(283, 188);
			this->textBox_duration->Name = L"textBox_duration";
			this->textBox_duration->ReadOnly = true;
			this->textBox_duration->Size = System::Drawing::Size(41, 22);
			this->textBox_duration->TabIndex = 206;
			// 
			// textBox_center
			// 
			this->textBox_center->Location = System::Drawing::Point(283, 218);
			this->textBox_center->Name = L"textBox_center";
			this->textBox_center->ReadOnly = true;
			this->textBox_center->Size = System::Drawing::Size(41, 22);
			this->textBox_center->TabIndex = 207;
			// 
			// textBox_susceptibility
			// 
			this->textBox_susceptibility->Location = System::Drawing::Point(283, 249);
			this->textBox_susceptibility->Name = L"textBox_susceptibility";
			this->textBox_susceptibility->ReadOnly = true;
			this->textBox_susceptibility->Size = System::Drawing::Size(41, 22);
			this->textBox_susceptibility->TabIndex = 208;
			// 
			// textBox_similiarStepMax
			// 
			this->textBox_similiarStepMax->Location = System::Drawing::Point(283, 292);
			this->textBox_similiarStepMax->Name = L"textBox_similiarStepMax";
			this->textBox_similiarStepMax->ReadOnly = true;
			this->textBox_similiarStepMax->Size = System::Drawing::Size(41, 22);
			this->textBox_similiarStepMax->TabIndex = 209;
			// 
			// textBox_contraryStepMax
			// 
			this->textBox_contraryStepMax->Location = System::Drawing::Point(283, 323);
			this->textBox_contraryStepMax->Name = L"textBox_contraryStepMax";
			this->textBox_contraryStepMax->ReadOnly = true;
			this->textBox_contraryStepMax->Size = System::Drawing::Size(41, 22);
			this->textBox_contraryStepMax->TabIndex = 210;
			// 
			// textBox_stepMax
			// 
			this->textBox_stepMax->Location = System::Drawing::Point(283, 355);
			this->textBox_stepMax->Name = L"textBox_stepMax";
			this->textBox_stepMax->ReadOnly = true;
			this->textBox_stepMax->Size = System::Drawing::Size(41, 22);
			this->textBox_stepMax->TabIndex = 211;
			// 
			// textBox_skipMax
			// 
			this->textBox_skipMax->Location = System::Drawing::Point(283, 388);
			this->textBox_skipMax->Name = L"textBox_skipMax";
			this->textBox_skipMax->ReadOnly = true;
			this->textBox_skipMax->Size = System::Drawing::Size(41, 22);
			this->textBox_skipMax->TabIndex = 212;
			// 
			// button_execute
			// 
			this->button_execute->Location = System::Drawing::Point(332, 491);
			this->button_execute->Name = L"button_execute";
			this->button_execute->Size = System::Drawing::Size(166, 23);
			this->button_execute->TabIndex = 1;
			this->button_execute->Text = L"Execute";
			this->button_execute->UseVisualStyleBackColor = true;
			this->button_execute->Click += gcnew System::EventHandler(this, &Form1::button_execute_Click);
			// 
			// trackBar_countMax
			// 
			this->trackBar_countMax->Location = System::Drawing::Point(12, 458);
			this->trackBar_countMax->Maximum = 16;
			this->trackBar_countMax->Minimum = 4;
			this->trackBar_countMax->Name = L"trackBar_countMax";
			this->trackBar_countMax->Size = System::Drawing::Size(265, 42);
			this->trackBar_countMax->TabIndex = 114;
			this->trackBar_countMax->Value = 8;
			this->trackBar_countMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_countMax_Scroll);
			// 
			// textBox_countMax
			// 
			this->textBox_countMax->Location = System::Drawing::Point(283, 458);
			this->textBox_countMax->Name = L"textBox_countMax";
			this->textBox_countMax->ReadOnly = true;
			this->textBox_countMax->Size = System::Drawing::Size(41, 22);
			this->textBox_countMax->TabIndex = 214;
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(330, 458);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(139, 12);
			this->label13->TabIndex = 314;
			this->label13->Text = L"Count times maximum (2^n)";
			// 
			// trackBar_repetitionMax
			// 
			this->trackBar_repetitionMax->Location = System::Drawing::Point(12, 418);
			this->trackBar_repetitionMax->Maximum = 4;
			this->trackBar_repetitionMax->Name = L"trackBar_repetitionMax";
			this->trackBar_repetitionMax->Size = System::Drawing::Size(272, 42);
			this->trackBar_repetitionMax->TabIndex = 113;
			this->trackBar_repetitionMax->Scroll += gcnew System::EventHandler(this, &Form1::trackBar_repetitionMax_Scroll);
			// 
			// textBox_repetitionMax
			// 
			this->textBox_repetitionMax->Location = System::Drawing::Point(283, 418);
			this->textBox_repetitionMax->Name = L"textBox_repetitionMax";
			this->textBox_repetitionMax->ReadOnly = true;
			this->textBox_repetitionMax->Size = System::Drawing::Size(41, 22);
			this->textBox_repetitionMax->TabIndex = 213;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(330, 418);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(138, 12);
			this->label14->TabIndex = 313;
			this->label14->Text = L"Repetition time(s) maximum";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(547, 528);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->textBox_repetitionMax);
			this->Controls->Add(this->trackBar_repetitionMax);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->textBox_countMax);
			this->Controls->Add(this->trackBar_countMax);
			this->Controls->Add(this->button_execute);
			this->Controls->Add(this->textBox_skipMax);
			this->Controls->Add(this->textBox_stepMax);
			this->Controls->Add(this->textBox_contraryStepMax);
			this->Controls->Add(this->textBox_similiarStepMax);
			this->Controls->Add(this->textBox_susceptibility);
			this->Controls->Add(this->textBox_center);
			this->Controls->Add(this->textBox_duration);
			this->Controls->Add(this->textBox_tonic);
			this->Controls->Add(this->textBox_mode);
			this->Controls->Add(this->textBox_range);
			this->Controls->Add(this->textBox_bottom);
			this->Controls->Add(this->textBox_nNote);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->trackBar_skipMax);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->trackBar_stepMax);
			this->Controls->Add(this->trackBar_contraryStepMax);
			this->Controls->Add(this->trackBar_similiarStepMax);
			this->Controls->Add(this->trackBar_susceptibility);
			this->Controls->Add(this->trackBar_center);
			this->Controls->Add(this->trackBar_duration);
			this->Controls->Add(this->trackBar_tonic);
			this->Controls->Add(this->trackBar_mode);
			this->Controls->Add(this->trackBar_range);
			this->Controls->Add(this->trackBar_bottom);
			this->Controls->Add(this->trackBar_nNote);
			this->Name = L"Form1";
			this->Text = L"Melody";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_nNote))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_bottom))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_range))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_mode))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_tonic))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_duration))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_center))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_susceptibility))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_similiarStepMax))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_contraryStepMax))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_stepMax))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_skipMax))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_countMax))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->trackBar_repetitionMax))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();
		}
#pragma endregion
private:
System::Void trackBar_nNote_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_nNote->Text = this->trackBar_nNote->Value.ToString();
}
System::Void trackBar_bottom_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_bottom->Text = this->trackBar_bottom->Value.ToString();
}
System::Void trackBar_range_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_range->Text = this->trackBar_range->Value.ToString();
}
System::Void trackBar_mode_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_mode->Text = this->trackBar_mode->Value.ToString();
}
System::Void trackBar_tonic_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_tonic->Text = this->trackBar_tonic->Value.ToString();
}
System::Void trackBar_duration_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_duration->Text = this->trackBar_duration->Value.ToString();
}
System::Void trackBar_center_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_center->Text = this->trackBar_center->Value.ToString();
}
System::Void trackBar_susceptibility_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_susceptibility->Text = this->trackBar_susceptibility->Value.ToString();
}
System::Void trackBar_similiarStepMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_similiarStepMax->Text = this->trackBar_similiarStepMax->Value.ToString();
	// settings consistency control:
	if(this->trackBar_stepMax->Value < this->trackBar_similiarStepMax->Value) {
		this->trackBar_stepMax->Value = this->trackBar_similiarStepMax->Value;
		this->textBox_stepMax->Text = this->trackBar_stepMax->Value.ToString();
	}
	else;
}
System::Void trackBar_contraryStepMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_contraryStepMax->Text = this->trackBar_contraryStepMax->Value.ToString();
	// settings consistency control:
	if(this->trackBar_stepMax->Value < this->trackBar_contraryStepMax->Value) {
		this->trackBar_stepMax->Value = this->trackBar_contraryStepMax->Value;
		this->textBox_stepMax->Text = this->trackBar_stepMax->Value.ToString();
	}
	else;
}
System::Void trackBar_stepMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_stepMax->Text = this->trackBar_stepMax->Value.ToString();
	// settings consistency control:
	if(this->trackBar_similiarStepMax->Value > this->trackBar_stepMax->Value) {
		this->trackBar_similiarStepMax->Value = this->trackBar_stepMax->Value;
		this->textBox_similiarStepMax->Text = this->trackBar_similiarStepMax->Value.ToString();
	}
	else;
	if(this->trackBar_contraryStepMax->Value > this->trackBar_stepMax->Value) {
		this->trackBar_contraryStepMax->Value = this->trackBar_stepMax->Value;
		this->textBox_contraryStepMax->Text = this->trackBar_contraryStepMax->Value.ToString();
	}
	else;
}
System::Void trackBar_skipMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_skipMax->Text = this->trackBar_skipMax->Value.ToString();
}
System::Void trackBar_repetitionMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_repetitionMax->Text = this->trackBar_repetitionMax->Value.ToString();
}
System::Void trackBar_countMax_Scroll(System::Object^  sender, System::EventArgs^  e) {
	this->textBox_countMax->Text = this->trackBar_countMax->Value.ToString();
}
System::Void button_execute_Click(System::Object^  sender, System::EventArgs^  e)
{
	short nNote = this->trackBar_nNote->Value;
	short bottom = this->trackBar_bottom->Value;
	short range= this->trackBar_range->Value;
	short mode = this->trackBar_mode->Value;
	short tonic = this->trackBar_tonic->Value;
	short duration= this->trackBar_duration->Value;
	short center= this->trackBar_center->Value;
	short susceptibility = this->trackBar_susceptibility->Value;
	short similiarStepMax = this->trackBar_similiarStepMax->Value;
	short contraryStepMax = this->trackBar_contraryStepMax->Value;
	short stepMax = this->trackBar_stepMax->Value;
	short skipMax = this->trackBar_skipMax->Value;
	short repetitionMax = this->trackBar_repetitionMax->Value;
	short exponent = this->trackBar_countMax->Value;
	double countMax = pow(2.0, exponent);
	// main function:
	short* pitches = new short[nNote]; // pitch array
	Note note(nNote, bottom, range, mode, tonic, center, susceptibility,
			  similiarStepMax, contraryStepMax, stepMax, skipMax, repetitionMax, countMax);
	if(isComplete(nNote, pitches, countMax, note))
		finalScore(nNote, pitches, duration); // write the result score
	else
		finalScore(); // write a blank score
	delete [] pitches;
	pitches = NULL;
}
bool isComplete(short nNote, short* pitches, double countMax, Note note)
{
	bool is = false;
	double count = 0;
	short i = 0;
	do
	{
		if(i <= 0) // 1. (re)start the first pitch
		{
			i = 0;
			pitches[i] = note.compose();
		}
		else if(i == 1) // 2. the second pitch
			pitches[i] = note.compose(pitches[0]);
		else // 3. following pitches
			pitches[i] = note.compose(pitches[i-2], pitches[i-1]);		
		// backtrack procedure:
		if(pitches[i] < 0) // rejectee (nextPitch_ == -1)
			i--; // move to the previous pitch
		else // candidate
			i++; // move to the next pitch
		// main count times threshold:
		count++;
		if(count > countMax)
		{
			is = false;
			break;
		}
		else
			is = true;
	} while(i<nNote);
	return is;
}
void finalScore(short nNote, short* pitches, short duration) // write the result score
{
	short noteon = 0;
	short pitch = 0;
	Score score;
	score.create();
	for(short i=0; i<nNote; i++)
	{
		pitch = pitches[i];
		Score score(noteon, pitch, 127, 120, 0); // Noteon, Pitch, Velocity=127, Artdur=120, Channel=0
		score.write();
		noteon += duration; // whole note == 480
	}
	score.finish();
}
void finalScore() // write a blank score
{
	Score score;
	score.create();
	score.write();
	score.finish();
}
};
}
// Revision Notes

// 2013-12-02

// isComplete(short nNote, short* pitches, double countMax, Note note)
// combined "i<0" with "i==0" into "i<=0"

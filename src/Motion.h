// Melody Motion Identifier
#pragma once

class Motion
{
	public:
		Motion();
		Motion(short previousPitch, short presentPitch, short nextPitch);
		short identify(); // motion types identifier
	private:
		short previousPitch_;    // 0 ~ 127
		short presentPitch_;   // 0 ~ 127
		short nextPitch_;    // 0 ~ 127
		short previousInterval_; // -127 ~ +127
		short nextInterval_; // -127 ~ +127
		bool areSimilarDirections_; // T: similiar, F: contrary
		short previousMotion_;   // 0: repetition, 1: step, 2: skip
		short nextMotion_;   // 0: repetition, 1: step, 2: skip
};

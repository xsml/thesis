// Score Writer for the File Format of MusicSculptor by Phil Winsor
#pragma once

class Score
{
	public:
		Score();
		Score(short noteon, short pitch, short velocity, short artdur, short channel);
		void create(); // output file initiator
		void write(); // note writer
		void finish(); // output file closer
	private:
		short noteon_;
		short pitch_;
		short velocity_;
		short artdur_;
		short channel_;
};
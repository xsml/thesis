#include "stdafx.h"
#include "Motion.h"

Motion::Motion()
{
	previousPitch_ = 0;
	presentPitch_ = 0;
	nextPitch_ = 0;
	previousInterval_ = 0;
	nextInterval_ = 0;
	areSimilarDirections_ = false;
	previousMotion_ = 0;
	nextMotion_ = 0;
}
Motion::Motion(short previousPitch, short presentPitch, short nextPitch)
{
    previousPitch_ = previousPitch;
    presentPitch_ = presentPitch;
    nextPitch_ = nextPitch;
    previousInterval_ = presentPitch_ - previousPitch_;
    nextInterval_ = nextPitch_ - presentPitch_;
	// directions:
    if(previousInterval_ * nextInterval_ >= 0) // not "> 0"
        areSimilarDirections_ = true; // similiar
    else
        areSimilarDirections_ = false; // contrary
	// previous motion:
    if(previousInterval_ == 0)
        previousMotion_ = 1; // repetition
    else if(previousInterval_ <= 2 && previousInterval_ >= -2)
		previousMotion_ = 2; // step
    else
        previousMotion_ = 3; // skip
	// next motion:
    if(nextInterval_ == 0)
        nextMotion_ = 1; // repetition
    else if(nextInterval_ <= 2 && nextInterval_ >= -2)
        nextMotion_ = 2; // step
    else     
        nextMotion_ = 3; // skip
}
short Motion::identify()
{
    short identity = 0;
	if(previousMotion_ == 2 && nextMotion_ == 2)
	{
		switch(areSimilarDirections_)
		{
			case true : identity = 1; break;// consecutive similiar step
			case false : identity = 2; break;// consecutive contrary step
		}
	}
	else if(previousMotion_ == 3 && nextMotion_ == 3)
		identity = 3; // consecutive skip
	else
		identity = 4; // none of above
	return identity;
}
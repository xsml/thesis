// Note Controller
#pragma once
#include "Motion.h"

class Note
{
	public:
		Note();
		Note(short nNote, short bottom, short range, short mode,
			 short tonic, short centerPitch, short susceptibility,
			 short consecutiveSimiliarStepTimesMax,
			 short consecutiveContraryStepMax,
			 short consecutiveStepTimesMax, short consecutiveSkipTimesMax,
			 short repetitionTimesMax, double countMax);
		short compose(); // first note creator
		short compose(short firstPitch); // second note creator
		short compose(short previousPitch, short presentPitch); // following note creator
		bool isOnModeScale(); // pitch check: mode scale
		bool isTolerableEnergyRatio(); // pitch check: energy ratio
		bool isAcceptableInterval(); // pitch check: interval
		bool isWithinRepetitionLimit(); // repetition times check
		bool isWithinConsecutiveMotionLimit(); // consecutive motion check
		void countRepetitionTimes(); // repetition times counter
		void countConsecutiveMotion(Motion motion); // consecutive motion counter
	private:
		short nNote_; // note amount
		short bottom_; // bottom pitch
		short range_; // melody range
		short mode_; // mode: 1.Ionian, 2.Aeolian
		short tonic_; // tonic pitch
		short centerPitch_; // center pitch
		short susceptibility_; // susceptibility constant
		short consecutiveSimiliarStepTimesMax_; // consecutive similiar step maximum
		short consecutiveContraryStepTimesMax_; // consecutive contrary step maximum
		short consecutiveStepTimesMax_; // consecutive step maximum
		short consecutiveSkipTimesMax_; // consecutive skip maximum
		short repetitionTimesMax_; // repetition maximum
		double countMax_; // count times maximum
		short previousPitch_; // the previous pitch
		short presentPitch_; // the present pitch
		short nextPitch_; // the next pitch
		double nextEnergyRatio_; // next energy ratio
		double energyRatioMax_; // tolerable energy ratio maximum
		double energyRatioMin_; // tolerable energy ratio minmum
		double tempEnergyRatioMax_; // temporary tolerable energy ratio maximum
		double tempEnergyRatioMin_; // temporary tolerable energy ratio minmum
		short nextInterval_; // next pitch interval
		short consecutiveSimiliarStepTimes_; // consecutive similiar step times
		short consecutiveContraryTimes_; // consecutive contrary step times
		short consecutiveStepTimes_; // consecutive step times
		short consecutiveSkipTimes_; // consecutive skip times
		short repetitionTimes_; // repetition times
		short tempConsecutiveSimiliarStepTimes_; // temporary consecutive similiar step times
		short tempConsecutiveContraryStepTimes_; // temporary consecutive contrary step times
		short tempConsecutiveStepTimes_; // temporary consecutive step times
		short tempConsecutiveSkipTimes_; // temporary consecutive skip times
		short tempRepetitionTimes_; // temporary repetition times
};
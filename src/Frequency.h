// Pitch Frequency Giver
#pragma once

class Frequency
{
	public:
		Frequency();
		Frequency(short mode, short tonic, short susceptibility,
				  short centerPitch, short presentPitch, short nextPitch);
		double allocateFrequency(short octave, short pitchClass);
		double countEnergyRatioMax(double energyRatioMax); // Max
		double countEnergyRatioMin(double energyRatioMin); // Min = 1 / rMin
		double getNextEnergyRatio();
	private:
		short mode_; // mode: 1.Ionian, 2.Aeolian
		short tonic_; // tonic pitch
		short susceptibility_; // susceptibility constant
		short centerPitch_; // centerPitch
		short presentPitch_; // present pitch
		short nextPitch_; // next pitch
		short centerOctave_; // center pitch register location
		short presentOctave_ ; // present pitch register location
		short nextOctave_ ; // next pitch register location
		short centerPitchClass_; // center pitch class
		short presentPitchClass_; // present pitch class
		short nextPitchClass_; // next pitch class
		double centerFrequency_; // center pitch frequency
		double presentFrequency_; // present pitch frequency
		double nextFrequency_; // next pitch frequency
		double presentFrequencyRatio_; // present frequency ratio: frequency (n-1) / frequency (0)
		double nextFrequencyRatio_; // next frequency ratio: frequency (n) / frequency (0)
		double presentEnergyRatio_; // present energy ratio: (frequency (n-1) / frequency (0))^2
		double nextEnergyRatio_; // next energy ratio: (frequency (n) / frequency (0))^2
		double energyRatioInterval_; // energy ratio interval
};
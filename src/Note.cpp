#include "stdafx.h"
#include "Note.h"
#include "Frequency.h"

Note::Note()
{
	srand(time(NULL));
	nNote_ = 0;
	bottom_ = 0;
	range_= 0;
	mode_ = 0;
	tonic_ = 0;
	centerPitch_= 0;
	susceptibility_ = 0;
	consecutiveSimiliarStepTimesMax_ = 0;
	consecutiveContraryStepTimesMax_ = 0;
	consecutiveStepTimesMax_ = 0;
	consecutiveSkipTimesMax_ = 0;
	repetitionTimesMax_ = 0;
	countMax_ = 0;
	previousPitch_ = 0;
	presentPitch_ = 0;
	nextPitch_ = 0;
	nextEnergyRatio_ = 0;
	energyRatioMax_ = 0;
	energyRatioMin_ = 0;
	tempEnergyRatioMax_ = 0;
	tempEnergyRatioMin_ = 0;
	nextInterval_ = 0;
	consecutiveSimiliarStepTimes_ = 0;
	consecutiveContraryTimes_ = 0;
	consecutiveStepTimes_ = 0;
	consecutiveSkipTimes_ = 0;
	repetitionTimes_ = 0;
	tempConsecutiveSimiliarStepTimes_ = 0;
	tempConsecutiveContraryStepTimes_ = 0;
	tempConsecutiveStepTimes_ = 0;
	tempConsecutiveSkipTimes_ = 0;
	tempRepetitionTimes_ = 0;
}
Note::Note(short nNote, short bottom, short range, short mode, short tonic,
		   short centerPitch, short susceptibility,
		   short consecutiveSimiliarStepTimesMax, short consecutiveContraryStepMax,
		   short consecutiveStepTimesMax, short consecutiveSkipTimesMax,
		   short repetitionTimesMax, double countMax)
{
	srand(time(NULL));
	nNote_ = nNote;
	bottom_ = bottom;
	range_= range;
	mode_ = mode;
	tonic_ = tonic;
	centerPitch_= centerPitch;
	susceptibility_ = susceptibility;
	consecutiveSimiliarStepTimesMax_ = consecutiveSimiliarStepTimesMax;
	consecutiveContraryStepTimesMax_ = consecutiveContraryStepMax;
	consecutiveStepTimesMax_ = consecutiveStepTimesMax;
	consecutiveSkipTimesMax_ = consecutiveSkipTimesMax;
	repetitionTimesMax_ = repetitionTimesMax;
	countMax_ = countMax;
	previousPitch_ = 0;
	presentPitch_ = 0;
	nextPitch_ = 0;
	nextEnergyRatio_ = 0;
	energyRatioMax_ = 4.0; // initial Max = 4
	energyRatioMin_ = 0.25; // initial rMin = 4, therefore Min = 1/4 = 0.25
	tempEnergyRatioMax_ = 0;
	tempEnergyRatioMin_ = 0;
	nextInterval_ = 0;
	consecutiveSimiliarStepTimes_ = 0;
	consecutiveContraryTimes_ = 0;
	consecutiveStepTimes_ = 0;
	consecutiveSkipTimes_ = 0;
	repetitionTimes_ = 0;
	tempConsecutiveSimiliarStepTimes_ = 0;
	tempConsecutiveContraryStepTimes_ = 0;
	tempConsecutiveStepTimes_ = 0;
	tempConsecutiveSkipTimes_ = 0;
	tempRepetitionTimes_ = 0;
}

short Note::compose() // the first pitch
{
	presentPitch_ = centerPitch_; // present pitch is the center pitch; next pitch is the real first pitch
	double count = 0;
	do 
	{
		if(count > countMax_) // backtrack threshold
		{
			nextPitch_ = -1; // rejectee
			break;
		}
		else
		{
			do nextPitch_ = rand() % range_ + bottom_;
			while(!isOnModeScale());
			Frequency frequency(mode_, tonic_, susceptibility_, centerPitch_, presentPitch_, nextPitch_);
			nextEnergyRatio_ = frequency.getNextEnergyRatio();
			tempEnergyRatioMax_ = frequency.countEnergyRatioMax(energyRatioMax_);
			tempEnergyRatioMin_ = frequency.countEnergyRatioMin(energyRatioMin_);
			count++; // count times + 1
		}
	} while(!isTolerableEnergyRatio());
	if(nextPitch_ == -1); // rejectee
	else // candidate
	{
		energyRatioMax_ = tempEnergyRatioMax_;
		energyRatioMin_ = tempEnergyRatioMin_;
	}
	return nextPitch_;
}
short Note::compose(short presentPitch) // the second pitch
{
	presentPitch_ = presentPitch;
	double count = 0;
	do
	{
		if(count > countMax_) // backtrack threshold
		{
			nextPitch_ = -1; // rejectee
			break;
		}
		else
		{
			do nextPitch_ = rand() % range_ + bottom_;
			while(!isOnModeScale());
			Frequency frequency(mode_, tonic_, susceptibility_, centerPitch_, presentPitch_, nextPitch_);
			nextEnergyRatio_ = frequency.getNextEnergyRatio();
			tempEnergyRatioMax_ = frequency.countEnergyRatioMax(energyRatioMax_);
			tempEnergyRatioMin_ = frequency.countEnergyRatioMin(energyRatioMin_);
			countRepetitionTimes();
			count++; // count times + 1
		}
	} while(!isTolerableEnergyRatio() || !isAcceptableInterval() || !isWithinRepetitionLimit());
	if(nextPitch_ == -1); // rejectee
	else // candidate
	{
		energyRatioMax_ = tempEnergyRatioMax_;
		energyRatioMin_ = tempEnergyRatioMin_;
		repetitionTimes_ = tempRepetitionTimes_;
	}
	return nextPitch_;
}
short Note::compose(short previousPitch, short presentPitch) // following pitches
{
	previousPitch_ = previousPitch;
	presentPitch_ = presentPitch;
	double count = 0;
	do
	{
		if(count > countMax_) // backtrack threshold
		{
			nextPitch_ = -1; // rejectee
			break;
		}
		else
		{
			do nextPitch_ = rand() % range_ + bottom_;
			while(!isOnModeScale());
			// reset variables of consecutive motion:
			tempConsecutiveSimiliarStepTimes_ = consecutiveSimiliarStepTimes_;
			tempConsecutiveContraryStepTimes_ = consecutiveContraryTimes_;
			tempConsecutiveStepTimes_ = consecutiveStepTimes_;
			tempConsecutiveSkipTimes_ = consecutiveSkipTimes_;
			tempRepetitionTimes_ = repetitionTimes_;
			// contiune:
			Frequency frequency(mode_, tonic_, susceptibility_, centerPitch_, presentPitch_, nextPitch_);
			nextEnergyRatio_ = frequency.getNextEnergyRatio();
			tempEnergyRatioMax_ = frequency.countEnergyRatioMax(energyRatioMax_);
			tempEnergyRatioMin_ = frequency.countEnergyRatioMin(energyRatioMin_);
			Motion motion(previousPitch_, presentPitch_, nextPitch_);
			countRepetitionTimes();
			countConsecutiveMotion(motion);
			count++; // count times + 1
		}
	} while(!isTolerableEnergyRatio() || !isAcceptableInterval() || !isWithinRepetitionLimit() || !isWithinConsecutiveMotionLimit());
	if(nextPitch_ == -1); // rejectee
	else // candidate
	{
		consecutiveSimiliarStepTimes_ = tempConsecutiveSimiliarStepTimes_;
		consecutiveContraryTimes_ = tempConsecutiveContraryStepTimes_;
		consecutiveStepTimes_ = tempConsecutiveStepTimes_;
		consecutiveSkipTimes_ = tempConsecutiveSkipTimes_;
		repetitionTimes_ = tempRepetitionTimes_;
		energyRatioMax_ = tempEnergyRatioMax_;
		energyRatioMin_ = tempEnergyRatioMin_;
	}
	return nextPitch_;
}
bool Note::isOnModeScale()
{
    bool is = false;
	short pitchClass = (nextPitch_ + 12 - tonic_) % 12;
    switch(mode_)
	{
        case 1 : // Ionian
            if
			(
                pitchClass == 0  || pitchClass == 2  ||  pitchClass == 4  ||
                pitchClass == 5  || pitchClass == 7  ||  pitchClass == 9  ||
				pitchClass == 11
            )
                is = true;
            else
                is = false;
            break;
        case 2 : // Aeolian
            if
			(
                pitchClass == 0  || pitchClass == 2  || pitchClass == 3  ||
                pitchClass == 5  || pitchClass == 7  || pitchClass == 8  ||
				pitchClass == 10
            )
                is = true;
            else
                is = false;
            break;
        default :
            is = false;
            break;
    }
    return is;
}
bool Note::isTolerableEnergyRatio()
{
	bool is = false;
	if(nextEnergyRatio_ > tempEnergyRatioMax_)
		is = false;
	else if(nextEnergyRatio_ < tempEnergyRatioMin_)
		is = false;
	else
		is = true;
	return is;
}
bool Note::isAcceptableInterval()
{
	bool is = false;
	short interval = nextPitch_ - presentPitch_;
	if(interval > 12) // exceed ascending octave
		is = false;
	else if(interval < -12) // exceed descending octave
		is = false;
	else if(interval == 6) // ascending tritone
		is = false;
	else if(interval == -6) // descending tritone
		is = false;
	else if(interval == 10) // ascending minor seven
		is = false;
	else if(interval == -10) // descending minor seven
		is = false;
	else if(interval == 11) // ascending major seven
		is = false;
	else if(interval == -11) // descending major seven
		is = false;
	else
		is = true;
	return is;
}
bool Note::isWithinRepetitionLimit()
{
    bool is = false;
	if(tempRepetitionTimes_ <= repetitionTimesMax_)
        is = true;
    else
        is = false; // over step, skip, or repetition times maximum
    return is;
}
bool Note::isWithinConsecutiveMotionLimit()
{
    bool is = false;
	if
	(
		tempConsecutiveSimiliarStepTimes_ <= consecutiveSimiliarStepTimesMax_ &&
		tempConsecutiveContraryStepTimes_ <= consecutiveContraryStepTimesMax_ &&
		tempConsecutiveStepTimes_ <= consecutiveStepTimesMax_ && 
		tempConsecutiveSkipTimes_ <= consecutiveSkipTimesMax_
	)
        is = true;
    else
        is = false; // over step, skip, or repetition times maximum
    return is;
}
void Note::countRepetitionTimes()
{
	nextInterval_ = nextPitch_ - presentPitch_;
    if(nextInterval_ == 0) // repetition
		tempRepetitionTimes_ = 1;
	else // no repetition
		tempRepetitionTimes_ = 0;
}
void Note::countConsecutiveMotion(Motion motion)
{
	short identity = motion.identify();
	switch(identity)
	{
		case 1 : // consecutive similiar step
			tempConsecutiveSimiliarStepTimes_++;
			tempConsecutiveContraryStepTimes_ = 0;
			tempConsecutiveStepTimes_++;
			tempConsecutiveSkipTimes_ = 0;
			break;
		case 2 : // consecutive contrary step
			tempConsecutiveSimiliarStepTimes_ = 0;
			tempConsecutiveContraryStepTimes_++;
			tempConsecutiveStepTimes_++;
			tempConsecutiveSkipTimes_ = 0;
			break;
		case 3 : // consecutive skip
			tempConsecutiveSimiliarStepTimes_ = 0;
			tempConsecutiveContraryStepTimes_ = 0;
			tempConsecutiveStepTimes_ = 0;
			tempConsecutiveSkipTimes_++;
			break;
		default : // none of above
			tempConsecutiveSimiliarStepTimes_ = 0;
			tempConsecutiveContraryStepTimes_ = 0;
			tempConsecutiveStepTimes_ = 0;
			tempConsecutiveSkipTimes_ = 0;
			break;
	}
}
// Revision Notes

// 2013-12-02

// Note::compose()
// Note::compose(short firstPitch)
// Note::compose(short previousPitch, short presentPitch)
// 1. moved if(count > countMax_) to the beginning
// 2. added "if(nextPitch_ == -1)" to skip or continue to update variables